
How to setup:

```
sudo apt install ffmpeg

cd ~/.local
python -m venv pyenv
mkdir ~/.local/bin
ln -s $HOME/.local/pyenv/bin/activate pyenv
cd ~/code/document
source pyenv
pip install --upgrade pip
pip install wheel pip
```

