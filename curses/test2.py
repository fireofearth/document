import curses
from curses import wrapper

def helloworld(scr):
    for i in range(1, 11):
        s = '10 divided by {} is {}'.format(i, 10/i)
        scr.addstr(i, 0, s)

def addchars(scr):
    """Move and place character at position.
    """
    for i in range(1, 11):
        scr.addstr(i, i, "X")
    for i in range(1, 11):
        scr.addstr(i, i+1, "O")

def getchars(scr):
    """Get character from key press and compare it.
    """
    scr.addstr(0, 0, "Hello")
    s = "Press any key: "
    scr.addstr(1, 0, s)
    ch = scr.getch()
    s = "Key pressed is " + chr(ch)
    scr.addstr(2, 0, s)
    s = "Using key'a'? " + ("yes" if ch == ord('a') else "no")
    scr.addstr(3, 0, s)

def getmaxwindowsz(scr):
    """Get the screen size
    """
    scr.addstr(0, 0, str(scr.getmaxyx()))

def showpad():
    pad = curses.newpad(100, 100)
    # These loops fill the pad with letters; addch() is
    # explained in the next section
    for y in range(0, 99):
        for x in range(0, 99):
            pad.addch(y,x, ord('a') + (x*x+y*y) % 26)
    
    # Displays a section of the pad in the middle of the screen.
    # (0,0) : coordinate of upper-left corner of pad area to display.
    # (5,5) : coordinate of upper-left corner of window area to be filled
    #         with pad content.
    # (20, 75) : coordinate of lower-right corner of window area to be
    #          : filled with pad content.
    pad.refresh( 0,0, 5,5, 20,75)

def main(scr):
    # Clear screen
    scr.clear()

    # getchars(scr)
    getmaxwindowsz(scr)

    # showpad()

    scr.refresh()
    scr.getkey()

wrapper(main)
