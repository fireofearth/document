import curses

class App:
    def __init__(self):
        self.stdscr = None

    def start(self):
    
        # intialize curses
        # - determine terminal type (what are the types?)
        # - sends setup codes to the terminal (what are the setup codes?)
        
        # stdscr : curses.window
        #   window object representing the entire screen
        stdscr = curses.initscr()
        print(type(stdscr), stdscr)
        
        # stop application from echoing the keys to the screen
        curses.noecho()
        
        # allow application to react to keys without Enter
        curses.cbreak()
        
        # return for special keys like arrows, home, etc as multibyte escape sequence.
        stdscr.keypad(True)

    def end(self):
        # reverse terminal settings we set at start
        curses.nocbreak()
        stdscr.keypad(False)
        curses.echo()

        # restore terminal to original operating mode
        curses.endwin()

app = App()
app.start()
app.end()

