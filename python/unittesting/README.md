# Unittest

## Introduction

Guide on writing unit tests with the built-in `unittest`.

## Quick-start

Run all tests with the default options with
`python -m unittest`
