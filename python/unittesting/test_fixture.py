
from testing.fixtures import BaseTest

import unittest

class WithFixture1Test(BaseTest):
    def test_simple(self):
        self.compare_list(self.my_list, [1])

class WithFixture2Test(BaseTest):
    def setUp(self):
        super().setUp()
        self.my_list.append(2)

    def test_override_set_up(self):
        self.compare_list(self.my_list, [1, 2])
