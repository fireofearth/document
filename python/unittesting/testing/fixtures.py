import unittest

class BaseTest(unittest.TestCase):
    def setUp(self):
        self.my_list = []
        self.my_list.append(1)

    def tearDown(self):
        self.my_list.pop()

    def compare_list(self, l1, l2):
        self.assertListEqual(l1, l2)
