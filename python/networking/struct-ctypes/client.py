from common import ExStruct, Option, parser
import struct
import socket

args = parser.parse_args()

host = "localhost"
port = 60101
with socket.socket(
    socket.AF_INET, socket.SOCK_STREAM
) as skt:
    skt.connect((host, port))
    if args.type == Option.STRUCT:
        msg = struct.pack("i", -5)
        msg += struct.pack("I", 5)
        msg += struct.pack("d", 123.45)
    elif args.type == Option.CTYPES:
        msg = ExStruct()
        msg.x = -5
        msg.y = 5
        msg.z = 123.45
    else:
        raise NotImplementedError()
    skt.sendall(msg)
