from common import ExStruct, Option, parser
import struct
import socket

args = parser.parse_args()

host = "localhost"
port = 60101
with socket.socket(
    socket.AF_INET, socket.SOCK_STREAM
) as skt:
    skt.bind((host, port))
    skt.listen(1)
    conn, addr = skt.accept()
    msg = conn.recv(16)
    if args.type == Option.STRUCT:
        x = struct.unpack("i", msg[0:4])[0]
        y = struct.unpack("I", msg[4:8])[0]
        z = struct.unpack("d", msg[8:16])[0]
    elif args.type == Option.CTYPES:
        msg = ExStruct.from_buffer_copy(msg)
        x = msg.x
        y = msg.y
        z = msg.z
    else:
        raise NotImplementedError()
    print(f"from server: received {x} {y} {z}")
    conn.close()

