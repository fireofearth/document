Demonstration of using TCP socket client and server.
Demonstrates how to interchange ctypes and structs.

```
python server.py [struct|ctypes]
python client.py [struct|ctypes]
```
