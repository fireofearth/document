import argparse
import ctypes

class ExStruct(ctypes.Structure):
    _fields_ = [
        ("x", ctypes.c_int),
        ("y", ctypes.c_uint),
        ("z", ctypes.c_double)
    ]

class Option:
    STRUCT = "struct"
    CTYPES = "ctypes"

parser = argparse.ArgumentParser()
parser.add_argument(
    "type",
    choices=["struct", "ctypes"]
)
