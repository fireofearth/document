import socket

host = "localhost"
port = 60101
with socket.socket() as skt:
    skt.connect((host, port))
    msg = "I'm the client asdf asdf"
    skt.sendall(msg.encode())
    msg = skt.recv(1024).decode()
    print("from server: ", msg)
