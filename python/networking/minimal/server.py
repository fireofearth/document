import socket

host = "localhost"
port = 60101
with socket.socket() as skt:
    skt.bind((host, port))
    skt.listen(1)
    conn, addr = skt.accept()
    msg = conn.recv(1024).decode()
    if msg:
        print("from client: ", msg)
        msg = "I'm the server test test"
        conn.send(msg.encode())
    conn.close()
