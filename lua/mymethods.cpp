
// C++ DDL module for Lua

extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

// don't need extern "C" here
// extern "C" int myadd2(lua_State* L) {
static int myadd2(lua_State* L) {
    int a = lua_tointeger(L, 1);
    int b = lua_tointeger(L, 2);
    int result = a + b;
    lua_pushinteger(L, result);
    return 1;
}

static int mysub2(lua_State* L) {
    int a = lua_tointeger(L, 1);
    int b = lua_tointeger(L, 2);
    int result = a - b;
    lua_pushinteger(L, result);
    return 1;
}

// How to expose C++ functions
// https://stackoverflow.com/questions/19381301/how-to-expose-c-functions-to-a-lua-script

// how to compile C++ for Lua
// https://stackoverflow.com/questions/73652349/how-do-i-get-the-lua-libraries-for-linux-so-that-i-can-use-them-in-c
// /usr/include/lua5.4/

/*
// get compile options
pkg-config --cflags lua
pkg-config --libs lua
// compile as DLL, don't need int main()
g++ \
    -Wall \
    -shared \
    -o mymethods.so \
    mymethods.cpp \
    -I/usr/include/lua5.4 \
    -L/usr/local/lib \
    -llua5.4
*/

// include in lua using
// http://www.troubleshooters.com/codecorn/lua/lua_lua_calls_c.htm
/*
require('mymethods')
myadd2(1,2)
mysub2(1,2)
*/

extern "C" int luaopen_mymethods(lua_State *L) {
    lua_register(L, "myadd2", myadd2);
    lua_register(L, "mysub2", mysub2);
    return 0;
}

