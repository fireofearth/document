
-- get length of a table
function len(t)
    local c = 0
    for _ in pairs(t) do c = c + 1 end
    return c
end

function printTitle(title)
    print()
    print("==========")
    io.write("= ", title, "\n")
    print("==========")
    print()
end

function printSection(section)
    io.write("> ", section, "\n")
end

NsFunctions = {}

function NsFunctions.factorial(n)
    if n == 0 then
        return 1
    else
        return n * factorial(n-1)
    end
end

function NsFunctions.fibonacci(n)
    if n == 0 then
        return 0
    elseif n == 1 then
        return 1
    else
        return fibonacci(n-2) + fibonacci(n-1)
    end
end

function NsFunctions.run()
    printTitle("Functions")
    factorial = NsFunctions.factorial
    fibonacci = NsFunctions.fibonacci
    -- eval and print factorials
    io.write("factorials 3! =", factorial(3), ", 4! =", factorial(4), "\n")
    
    -- eval and print fibonaccis
    io.write("fibonacci f(6) =", fibonacci(6), ", f(12) =", fibonacci(12), "\n")
end

NsStrings = {}

function NsStrings.run()
    printTitle("Strings")
    x = 1
    y = 0.5
    s = "x="..tostring(x)..", y="..tostring(y)
    print(s)
end

NsTables = {}

-- For non-array tables, the only way to get its length is by iterating over it
-- see:
-- https://stackoverflow.com/questions/2705793/how-to-get-number-of-entries-in-a-lua-table
function NsTables.run()
    printTitle("Tables")
    t = {["asdf"] = 1, ["qwerty"] = 2, ["ihop"] = 4}
    print("key 'asdf' to t is ", t["asdf"])
    print("t.asdf is ", t.asdf)
    print("length of t is ", len(t))
    print("iterating over t...")
    for k, v in pairs(t) do
        print("    ", k, v)
    end
end

NsClasses = {}

function NsClasses.trial1()
    printSection("Hello World for Lua classes")
    Dog = {}
    function Dog:new()
        newObj = {sound = "woof"}
        self.__index = self
        return setmetatable(newObj, self)
    end

    function Dog:makeSound()
        print(self.sound)
    end

    myDog = Dog:new()
    myDog:makeSound()
end

function NsClasses.trial2()
    printSection("getters and setters")
    A = {}
    function A:new(x, y)
        newObj = {x = x, y = y}
        self.__index = self
        return setmetatable(newObj, self)
    end
    function A:getX() return self.x end
    function A:setX(x) self.x = x end
    function A:getY() return self.y end
    function A:setY(y) self.y = y end

    a = A:new(1, "asdf")
    print("initial values...")
    print("a.x = " .. a:getX())
    print("a.y = " .. a:getY())
    print("setting new values...")
    a:setX(100)
    a:setY("qwerty")
    print("a.x = " .. a:getX())
    print("a.y = " .. a:getY())
end

function NsClasses.run()
    printTitle("Classes")
    NsClasses.trial1()
    NsClasses.trial2()
end

NsFunctions.run()
NsStrings.run()
NsTables.run()
NsClasses.run()

