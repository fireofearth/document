
function factorial(n)
    if n == 0 then
        return 1
    else
        return n * factorial(n-1)
    end
end

function fibonacci(n)
    if n == 0 then
        return 0
    elseif n == 1 then
        return 1
    else
        return fibonacci(n-2) + fibonacci(n-1)
    end
end

-- print hello world
print("hello world")

-- eval and print factorials
print("factorials 3! =", factorial(3), ", 4! =", factorial(4))

-- eval and print fibonaccis
print("fibonacci f(6) =", fibonacci(6), ", f(12) =", fibonacci(12))

a = {}
for i=1,100 do a[i] = i end
for i=1,3 do io.write(a[i], " ") end
io.write(#a ,"\n")

