#!/bin/bash

print_section () {
    printf "\n> $1\n"
}

# see: Are double square brackets [[ ]] preferable over single square brackets [ ] in Bash?
# https://stackoverflow.com/questions/669452/are-double-square-brackets-preferable-over-single-square-brackets-in-b
# see: What's the difference between [ and [[ in Bash?
# https://stackoverflow.com/questions/3427872/whats-the-difference-between-and-in-bash
# based on:
# https://stackoverflow.com/questions/18668556/how-can-i-compare-numbers-in-bash
try_conditionals () {
    print_section "logical conditionals"
    local my_vals=(2 4)
    for I in "${my_vals[@]}"; do
        printf "I=$I "; [ "$I" -gt 3 ] && echo "I > 3 is true" || echo "I > 3 is false"
    done

    my_val=
    [ -z "$my_val" ] && echo "my_val is not set" || echo "my_val is set to $my_val"
    my_val=12
    [ -z "$my_val" ] && echo "my_val is not set" || echo "my_val is set to $my_val"
}

try_conditional_string () {
    print_section "conditional string comparison"
    local my_prefix=/home/fireofearth
    local my_val=/home/fireofearth/asdf/qwerty
    [[ "$my_val" == "$my_prefix"* ]] && \
        echo "$my_val has prefix $my_prefix" || \
        echo "$my_val doesn't have prefix $my_prefix"
    [[ "$my_val" != "$my_prefix"* ]] && \
        echo "$my_val doesn't have prefix $my_prefix" || \
        echo "$my_val has prefix $my_prefix"

    local my_word="fireofearth"
    [[ "$my_val" == *"$my_word"* ]] && \
        echo "$my_val contains word $my_word" || \
        echo "$my_val doesn't contain word $my_word"

    my_val=/home/danky/asdf/qwerty
    [[ "$my_val" = "$my_prefix"* ]] && \
        echo "$my_val has prefix $my_prefix" || \
        echo "$my_val doesn't have prefix $my_prefix"
    [[ "$my_val" == *"$my_word"* ]] && \
        echo "$my_val contains word $my_word" || \
        echo "$my_val doesn't contain word $my_word"
}

try_case () {
    print_section "logical case"
    local my_val=c
    case "$my_val" in
        a) echo a ;;
        b) echo b ;;
        c) echo c ;;
    esac
}

# .
# ├── blobs
# │   └── blob.txt
# ├── blob.sha
# ├── blob.txt
# └── testing.bash
try_filesystem_getters (){
    print_section "filesystem getter operations"
    
    FILEPATH="blobs/blob.txt"
    DIRPATH="blobs"
    [ -f "$FILEPATH" ] && echo "$FILEPATH is file" || echo "$FILEPATH not file"
    [ ! -f "$FILEPATH" ] && echo "$FILEPATH not file" || echo "$FILEPATH is file"
    [ -f "$DIRPATH" ] && echo "$DIRPATH is file" || echo "$DIRPATH not file"
    [ -d "$FILEPATH" ] && echo "$FILEPATH is dir" || echo "$FILEPATH not dir"
    [ -d "$DIRPATH" ] && echo "$DIRPATH is dir" || echo "$DIRPATH not dir"
    FAKE_DIRPATH="asdf"
    [ ! -d "$FAKE_DIRPATH" ] && echo "$FAKE_DIRPATH not dir" || echo "$FAKE_DIRPATH is dir"

}

try_array_getters () {
    print_section "array getter operations"
    MY_LIST=(
        "The"
        "quick"
        "brown fox"
        "jumps over the lazy dog"
    )
    echo "the word at index 1 is ${MY_LIST[1]}"
    I=2
    echo "he word at index I=$I is ${MY_LIST[$I]}"
    echo "looping in the list..."
    for V in "${MY_LIST[@]}"; do
        echo "    $V"
    done
    echo "printing the list, reusing printf format..."
    printf '    %s\n' "${MY_LIST[@]}"
}

# based on:
# https://stackoverflow.com/questions/1951506/add-a-new-element-to-an-array-without-specifying-the-index-in-bash
try_array_setters () {
    print_section "array setter operations"

    MY_LIST=()

    echo "original, len=${#MY_LIST[@]}, looping in the list..."
    printf "    "; printf "%s " "${MY_LIST[@]}"; printf "\n"

    MY_LIST+=("three" "four")
    MY_LIST+=()
    MY_LIST+=("five")
    echo "after appending, len=${#MY_LIST[@]}, looping in the list..."
    printf "    "; printf "%s " "${MY_LIST[@]}"; printf "\n"
    
    MY_LIST[2]="tommy"
    echo "after modifying, len=${#MY_LIST[@]}, looping in the list..."
    printf "    "; printf "%s " "${MY_LIST[@]}"; printf "\n"
}

# based on:
# https://www.baeldung.com/linux/check-bash-array-contains-value
try_contain_in_list () {
    print_section "check values against a list"

    MY_LIST=("one" "two" "potatoes" "bananas" "three" "apples")

    MY_VALS=("three" "thr" "threethree" "three three")

    for MY_VAL in "${MY_VALS[@]}"; do
        echo "trying $MY_VAL"
        printf "    "
        [[ $(echo "${MY_LIST[@]}" | grep -F -w "$MY_VAL") ]] && echo "$MY_VAL is in list" || echo "$MY_VAL is not in list"
    done
}

try_contain_in_sz1_list () {
    print_section "check values against a list of size one"

    MY_LIST=("three")

    MY_VALS=("three" "thr" "threethree" "three three")

    for MY_VAL in "${MY_VALS[@]}"; do
        echo "trying $MY_VAL"
        printf "    "
        [[ $(echo "${MY_LIST[@]}" | grep -F -w "$MY_VAL") ]] && echo "$MY_VAL is in list" || echo "$MY_VAL is not in list"
    done
}

# usage
# try this with
try_getopts () {
    echo "arguments are: $@"
    while getopts "ab:c" arg; do
        case $arg in
            a)
                echo "got -a"
                ;;
            b)
                echo "got -b with $OPTARG"
                ;;
            c)
                echo "got -c"
                ;;
        esac
    done
    shift $((OPTIND-1))
    echo "rest of the arguments are: $@"
}

try_sha256sum () {
    print_section "sha256 sum operations"
    sha256sum blob.txt
    HASH=56293a80e0394d252e995f2debccea8223e4b5b2b150bee212729b3b39ac4d46
    MALFORMED_HASH=56293a80e0394d252e995f2debccea8223e4b5b2b15000000000000000000000
    echo "$HASH blob.txt" | sha256sum -c --status | echo "hash is correct"
    echo "$MALFORMED_HASH blob.txt" | sha256sum -c --status | echo "hash is incorrect"
}

# try_conditionals
# try_conditional_string
# try_case
# try_filesystem_getters
# try_array_getters
# try_array_setters
# try_contain_in_list
# try_contain_in_sz1_list
try_getopts $@
# try_sha256sum

