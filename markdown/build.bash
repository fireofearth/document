#!/bin/bash

# FORMAT=commonmark
FORMAT=markdown

pandoc -s -f $FORMAT -t pdf \
    -M linkcolor=blue \
    -M geometry:margin=0.5in \
    -o trial1.pdf trial1.md

pandoc -s -f $FORMAT -t html \
    --mathjax \
    -M title="Trial 1" \
    -o trial1.html \
    trial1.md

