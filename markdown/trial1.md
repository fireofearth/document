Markdown
===

Compiling
---

To compile Markdown to PDF using `pandoc` do:
```
pandoc -s -f markdown -t pdf \
    -M linkcolor=blue \
    -M geometry:margin=0.5in \
    -o trial1.pdf trial1.md
```
This supports checkboxes, tables and LaTeX math.

To compile Markdown to HTML using `pandoc` do:
```
pandoc -s -f markdown -t html \
    --mathjax \
    -M title="Trial 1" \
    -o trial1.html \
    trial1.md
```
To open use `firefox --new-tab trial1.html` or `brave -t trial1.html`.

Another Section
---

### Another Sub-section

The quick brown fox jumped over the lazy dog's back.

> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

One asterisk *italicizes* words.

Two asterisks **bolds** words.

Superscipt^1^ and subscript~2~.

Tilde ~~strikes out~~ words.

term
: definition definition definition definition definition.

This is a `code` line.

```
This is a code block.
```

* Item 1
* Item 2
* Item 3

1. Item 1
2. Item 2
3. Item 3

- [X] Task 1
- [ ] Task 2
- [ ] Task 3

Here's a [guide](https://daringfireball.net/projects/markdown/basics) to basic Markdown.

Here's a raw link <https://daringfireball.net/projects/markdown/basics>.

Here's a table

| Item | Description |
|------|------------:|
| 1    |         Red |
| 2    |        Fire |
| 3    |        Ball |

~~~mermaid
graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;
~~~

### Mathematics

Here's an equation: $x = \frac{y}{2} + z^2$ inline.

Here's a block equation
$$\int^1_0 xe^x dx$$
between lines.

Matrix in block equation
$$
A = \begin{bmatrix}
1 & 2 \\
3 & 4
\end{bmatrix}
$$

Aligned equations
\begin{align} 
2x - 5y &=  8 \\ 
3x + 9y &=  -12
\end{align}

